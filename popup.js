var titleSize = 45;

document.addEventListener('DOMContentLoaded', function () {    
    chrome.tabs.query({currentWindow:true}, function(tabs){
	var count = tabs.length;
	if(count > 1){
	    for(var i = 0; i < count; ++i){
		var div = document.createElement('div');
		var img = document.createElement('img');
		img.src = tabs[i].favIconUrl;
		div.appendChild(img);
		div.appendChild(document.createTextNode(tabs[i].title.slice(0, titleSize))); 
		div.onclick = function(id){return function(){chrome.tabs.update(id, {active:true, highlighted:true});};}(tabs[i].id);
		document.body.appendChild(div);
	    }
	}
    });
});
